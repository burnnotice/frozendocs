## Drone Code
#### This documentation will explain what the code and flowchart shows.

At the beginning, the drone is activated.  
Next, the drone locates its position.  
Then the drone locates the nearest person (who is not a teammate).  
If the view to the enemy is not obstructed, the drone will fire shots.  
Meanwhile, the enemy will locate the drone and, again, if the view is not obstructed, shots will be fired.  
Was the enemy/drone hit?  
If the drone was hit, keep shooting at it until destroyed.  
If the enemy was hit, ensure the enemy was destroyed.  
See results (Either the enemy was destroyed and the drone deactivates, or the drone was destroyed.)