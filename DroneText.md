##Drone Activity Interactions

The user will activate the drone.  
The drone will then report its location.  
The drone will locate the nearest person.  
If this person is a teammate, the drone will not fire and will find the next closest person.  
Once it finds an enemy, it will shoot at the enemy.  
The enemy will shoot back until the drone or the enemy are dead.  
If the enemy dies, then the drone will repeat the process of finding the nearest person.